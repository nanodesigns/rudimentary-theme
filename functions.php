<?php
/**
 * Rudimentary Theme Debugging Functions
 * @package rudimentary-theme
 * @author Mayeenul Islam <wz.islam@gmail.com>
 */

/**
 * Necessary Constants
 * to control the site from a single area
 * -----------------------------------------------------------------------------
 */
define( 'ENABLE_DEV', true ); //make it true to enable our development code
define( 'ENABLE_CPT', false ); //make it true to enable CPT and Custom Taxonomy
define( 'FILTER_DEBUG', false ); //make it true to debug filter hooks


//load necessary scripts as per need
if( ENABLE_DEV )
	require get_template_directory() .'/__development/__development.php';

if( ENABLE_CPT )
	require get_template_directory() .'/__cpt-taxonomy.php';



/**
 * Filter Debugging
 * @author  Andrew Nacin
 * @link https://nacin.com/2010/04/23/5-ways-to-debug-wordpress/
 * -----------------------------------------------------------------------------
 */
if( FILTER_DEBUG )
	add_action( 'all', create_function( '', 'var_dump( current_filter() );' ) );