<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="UTF-8">
		<title>Rudimentary theme for defaults debugging</title>
		<?php wp_head(); ?>
	</head>
	<body>

		<?php if( have_posts() ) : ?>

			<?php while ( have_posts() ) : the_post(); ?>

				<h2><?php the_title(); ?></h2>

			<?php endwhile; ?>

		<?php endif; ?>
		
		<?php wp_footer(); ?>
	</body>
</html>