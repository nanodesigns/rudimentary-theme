<?php
/**
 * Part of Debugging
 * @package rudimentary-theme
 * @author Mayeenul Islam <wz.islam@gmail.com>
 */

add_action( 'init', 'goromgorom_register_cpt_offers' );

function goromgorom_register_cpt_offers() {

    $labels = array( 
        'name'                  => 'Students',
        'singular_name'         => 'Student',
        'add_new'               => 'Add New',
        'add_new_item'          => 'Add New Student',
        'edit_item'             => 'Edit Student',
        'new_item'              => 'New Student',
        'view_item'             => 'View Student',
        'search_items'          => 'Search Students',
        'not_found'             => 'No Students found',
        'not_found_in_trash'    => 'No Students found in Trash',
        'parent_item_colon'     => 'Parent Student:',
        'menu_name'             => 'Students',
    );

    $args = array( 
        'labels'                => $labels,
        'hierarchical'          => false,
        'description'           => 'All types of Students to gather to the website',
        'supports'              => array( 'title', 'editor', 'author', 'excerpt' ),
        'taxonomies'            => array( 'classes' ),   
        'public'                => true,
        'show_ui'               => true,
        'show_in_menu'          => true,
        'menu_position'         => 7,
        'menu_icon'             => 'dashicons-welcome-learn-more',
        'show_in_nav_menus'     => true,
        'publicly_queryable'    => true,
        'exclude_from_search'   => false,
        'has_archive'           => true,
        'query_var'             => true,
        'can_export'            => true,
        'rewrite'               => array( 'slug' => 'students' ),
        //'capabilities'          => 'posts',
        'map_meta_cap'          => true
    );

    register_post_type( 'students', $args );
}*/


/**
 * Offer Categories - assigned to Offers
 * --------------------------------------------------------------------------
 */
add_action( 'init', 'goromgorom_create_offer_categories', 0 );

function goromgorom_create_offer_categories() {

    $labels = array(
        'name'              => 'Classes',
        'singular_name'     => 'Class',
        'search_items'      => 'Search Classes',
        'all_items'         => 'All Classes',
        'parent_item'       => 'Parent Class',
        'parent_item_colon' => 'Parent Class:',
        'edit_item'         => 'Edit Classes',
        'update_item'       => 'Update Classes',
        'add_new_item'      => 'Add New Class',
        'new_item_name'     => 'New Class Name',
        'menu_name'         => 'Classes',
    );

    $args = array(
        'hierarchical'      => true,
        'labels'            => $labels,
        'show_ui'           => true,
        'show_admin_column' => true,
        'query_var'         => true,
        'rewrite'           => array( 'slug' => 'classes' ),
    );
    register_taxonomy( 'classes', array( 'students' ), $args );

}